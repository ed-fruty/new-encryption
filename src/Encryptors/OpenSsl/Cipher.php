<?php

namespace Innovation\Encryption\Encryptors\OpenSsl;

use Innovation\Encryption\Exceptions\DecryptionException;
use Innovation\Encryption\ValueObjects\EncryptionKey;
use Innovation\Encryption\ValueObjects\Payload;

class Cipher
{
    /**
     * @var string
     */
    private $method;

    /**
     * Cipher constructor.
     * @param string $method
     */
    public function __construct($method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function generateIv()
    {
        return openssl_random_pseudo_bytes($this->getIvLength(), $crypto);
    }

    /**
     * @param string $payload
     * @param string $key
     * @param string $iv
     * @param int $options
     * @return string
     */
    public function encrypt($payload, $key, $iv, $options = OPENSSL_RAW_DATA)
    {
        return openssl_encrypt((string) $payload, $this->method, (string) $key, $options, $iv);
    }

    /**
     * @param string $encrypted
     * @param string $key
     * @param string $iv
     * @param int $options
     * @return string
     * @throws DecryptionException
     */
    public function decrypt($encrypted, $key, $iv, $options = OPENSSL_RAW_DATA)
    {
        $decrypted =  @openssl_decrypt($encrypted, $this->method, $key, $options, $iv);
        if (!$decrypted) {
            throw new DecryptionException(sprintf('Decryption failed. Error: %s', openssl_error_string()));
        }

        return $decrypted;
    }

    /**
     * @return int
     */
    public function getIvLength()
    {
        return openssl_cipher_iv_length($this->method);
    }
}