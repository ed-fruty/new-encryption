<?php

namespace Innovation\Encryption\Encryptors\OpenSsl;

use Innovation\Encryption\ValueObjects\EncryptionKey;

class Digest
{
    /**
     * @var string
     */
    private $algorithm;

    /**
     * Digest constructor.
     * @param string $algorithm
     */
    public function __construct($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @param EncryptionKey $key
     * @return EncryptionKey
     */
    public function update(EncryptionKey $key)
    {
        return new EncryptionKey(
            openssl_digest((string) $key, $this->algorithm)
        );
    }
}