<?php

namespace Innovation\Encryption\Encryptors\OpenSsl;

class Hmac
{
    const DEFAULT_LENGTH = 64;

    /**
     * @var string
     */
    private $algorithm;

    /**
     * Hmac constructor.
     * @param string $algorithm
     */
    public function __construct($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @param string $payload
     * @param string $key
     * @return string
     */
    public function make($payload, $key)
    {
        return hash_hmac($this->algorithm, (string) $payload, (string) $key);
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return static::DEFAULT_LENGTH;
    }

    /**
     * @param string $known
     * @param string $another
     * @return bool
     */
    public function equals($known, $another)
    {
        return hash_equals($known, $another);
    }
}