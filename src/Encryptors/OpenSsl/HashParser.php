<?php

namespace Innovation\Encryption\Encryptors\OpenSsl;

use Innovation\Encryption\ValueObjects\Hash;

class HashParser
{
    /**
     * @var string
     */
    private $charset;

    /**
     * HashParser constructor.
     * @param string $charset
     */
    public function __construct($charset)
    {
        $this->charset = $charset;
    }

    /**
     * @param Hash $hash
     * @param Cipher $cipher
     * @return string
     */
    public function parseIv(Hash $hash, Cipher $cipher)
    {
        return mb_substr($hash->toString(), 0, $cipher->getIvLength(), $this->charset);
    }

    /**
     * @param Hash $hash
     * @param Cipher $cipher
     * @param Hmac $hmac
     * @return string
     */
    public function parseHmac(Hash $hash, Cipher $cipher, Hmac $hmac)
    {
        return mb_substr($hash->toString(), $cipher->getIvLength(), $hmac->getLength(), $this->charset);
    }

    /**
     * @param Hash $hash
     * @param Cipher $cipher
     * @param Hmac $hmac
     * @return string
     */
    public function parseEncrypted(Hash $hash, Cipher $cipher, Hmac $hmac)
    {
        return mb_substr(
            $hash->toString(),
            $cipher->getIvLength() + $hmac->getLength(),
            mb_strlen($hash->toString(), $this->charset),
            $this->charset
        );
    }
}