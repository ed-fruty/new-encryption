<?php

namespace Innovation\Encryption\Encryptors\OpenSsl;

use Innovation\Encryption\EncryptionInterface;
use Innovation\Encryption\ValueObjects\EncryptionKey;
use Innovation\Encryption\ValueObjects\Hash;
use Innovation\Encryption\ValueObjects\Payload;

class OpenSslEncryption implements EncryptionInterface
{
    const CIPHER_METHOD = 'aes-256-cbc';
    const HMAC_ALGORITHM = 'sha256';
    const DIGEST_ALGORITHM = 'md5';
    const PARSER_CHARSET = 'ISO-8859-1';

    /**
     * @var Cipher
     */
    private $cipher;
    /**
     * @var Hmac
     */
    private $hmac;
    /**
     * @var Digest
     */
    private $digest;
    /**
     * @var HashParser
     */
    private $hashParser;

    /**
     * OpenSslEncryption constructor.
     * @param Cipher|null $cipher
     * @param Hmac|null $hmac
     * @param Digest|null $digest
     * @param HashParser|null $hashParser
     */
    public function __construct(
        Cipher $cipher = null,
        Hmac $hmac = null,
        Digest $digest = null,
        HashParser $hashParser = null
    ) {
        $this->cipher = $cipher ?: new Cipher(static::CIPHER_METHOD);
        $this->hmac = $hmac ?: new Hmac(static::HMAC_ALGORITHM);
        $this->digest = $digest ?: new Digest(static::DIGEST_ALGORITHM);
        $this->hashParser = $hashParser ?: new HashParser(static::PARSER_CHARSET);
    }

    public function encrypt(Payload $payload, EncryptionKey $key)
    {
        $key = $this->digest->update($key);
        $iv = $this->cipher->generateIv();

        $encrypted = $this->cipher->encrypt($payload->toString(), $key->toString(), $iv);
        $hmac = $this->hmac->make($payload->toString(), $key->toString());

        return (new Hash($iv . $hmac . $encrypted))->base64Encode();
    }

    /**
     * @param Hash $hash
     * @param EncryptionKey $key
     * @return Payload
     * @throws \Exception
     * @throws \Innovation\Encryption\Exceptions\DecryptionException
     */
    public function decrypt(Hash $hash, EncryptionKey $key)
    {
        $key = $this->digest->update($key);
        $hash = $hash->base64Decode();

        $parsedIv = $this->hashParser->parseIv($hash, $this->cipher);
        $parsedHmac = $this->hashParser->parseHmac($hash, $this->cipher, $this->hmac);
        $parsedEncrypted = $this->hashParser->parseEncrypted($hash, $this->cipher, $this->hmac);

        $decrypted = $this->cipher->decrypt($parsedEncrypted, $key, $parsedIv);

        $decryptedHmac = $this->hmac->make($decrypted, $key->toString());

        if (!$this->hmac->equals($decryptedHmac, $parsedHmac)) {
            throw new \Exception("Invalid integrity check");
        }        

        return new Payload($decrypted);
    }
}