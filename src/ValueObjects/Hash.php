<?php

namespace Innovation\Encryption\ValueObjects;

class Hash
{
    private $value;

    public function __construct($value)
    {
        $this->value = (string) $value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->value;
    }

    /**
     * @return static
     */
    public function base64Encode()
    {
        return new static(base64_encode($this->toString()));
    }

    /**
     * @return static
     */
    public function base64Decode()
    {
        return new static(base64_decode($this->toString()));
    }
}