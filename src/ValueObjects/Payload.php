<?php

namespace Innovation\Encryption\ValueObjects;

class Payload
{
    private $value;

    public function __construct($value)
    {
        $this->value = (string) $value;
    }

    public function __toString()
    {
        return $this->toString();
    }

    public function toString()
    {
        return $this->value;
    }
}