<?php

namespace Innovation\Encryption;

use Innovation\Encryption\ValueObjects\EncryptionKey;
use Innovation\Encryption\ValueObjects\Hash;
use Innovation\Encryption\ValueObjects\Payload;

interface EncryptionInterface
{
    /**
     * @param Payload $payload
     * @param EncryptionKey $key
     * @return Hash
     */
    public function encrypt(Payload $payload, EncryptionKey $key);

    /**
     * @param Hash $hash
     * @param EncryptionKey $key
     * @return Payload
     */
    public function decrypt(Hash $hash, EncryptionKey $key);
}