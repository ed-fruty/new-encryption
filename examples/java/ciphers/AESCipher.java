package ciphers;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Mac;

import java.util.Base64;
import java.security.*;
import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.URLDecoder;

public class AESCipher {

    private static String CIPHER_NAME = "AES/CBC/PKCS5PADDING";
    private static int CIPHER_KEY_LEN = 32; //256 bits
    private static String DIGEST_ALGORITHM = "MD5";
    private static String HMAC_ALGORITHM = "HmacSHA256";
    private static String CHARSET = "ISO-8859-1";
    private static int HMAC_LENGTH = 64;
    private static int IV_LENGTH = 16;

    
    public static String encrypt(String key, String data) {
        try {
            System.out.println("Encryption: Cipher name: " + CIPHER_NAME);
            System.out.println("Encryption: Cipher key length: " + CIPHER_KEY_LEN);
            System.out.println("Encryption: Charset: " + CHARSET);

            System.out.println("Encryption: Key digest algorithm: " + DIGEST_ALGORITHM);
            System.out.println("Encryption: Real key: " + key);

            key = digest(key);
            String iv = generateIv();

            System.out.println("Encryption: Key DIGEST: " + key);
            System.out.println("Encryption: Iv length: " + IV_LENGTH);
            System.out.println("Encryption: Generated Iv hex: " + bytesToHex(iv.getBytes(CHARSET)));

            IvParameterSpec initVector = new IvParameterSpec(iv.getBytes(CHARSET));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(CHARSET), "AES");

            Cipher cipher = Cipher.getInstance(AESCipher.CIPHER_NAME);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, initVector);

            byte[] encryptedData = cipher.doFinal((data.getBytes(CHARSET)));
            
            String hmac = hmac(data, key);

            System.out.println("Encryption: Hmac algorithm: " + HMAC_ALGORITHM);
            System.out.println("Encryption: Hmac length: " + HMAC_LENGTH);
            System.out.println("Encryption: Hmac value: " + hmac);

            String rawConcat =
                  new String(iv.getBytes(CHARSET), CHARSET)
                + new String(hmac.getBytes(CHARSET), CHARSET)
                + new String(encryptedData, CHARSET);

            String hash = Base64.getEncoder().encodeToString(rawConcat.getBytes(CHARSET));

            System.out.println("Encryption: Encoded hash: " + hash);

            return hash;
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String decrypt(String key, String data) {
        try {
            System.out.println("Decryption: Cipher name: " + CIPHER_NAME);
            System.out.println("Decryption: Cipher key length: " + CIPHER_KEY_LEN);
            System.out.println("Decryption: Charset: " + CHARSET);

            System.out.println("Decryption: Key digest algorithm: " + DIGEST_ALGORITHM);
            System.out.println("Decryption: Real key: " + key);

            key = digest(key);

            System.out.println("Decryption: Key DIGEST: " + key);
            System.out.println("Decryption: Iv length: " + IV_LENGTH);

            String rawHash = new String(Base64.getDecoder().decode(data), CHARSET);

            String parsedIv = rawHash.substring(0, IV_LENGTH);
            String parsedHmac = rawHash.substring(IV_LENGTH, IV_LENGTH + HMAC_LENGTH);
            String parsedEncrypted = rawHash.substring(IV_LENGTH + HMAC_LENGTH);

            System.out.println("Decryption: Parsed Iv hex: " + bytesToHex(parsedIv.getBytes(CHARSET)));
            System.out.println("Decryption: Parsed Hmac hex: " + parsedHmac);

            
            IvParameterSpec iv = new IvParameterSpec(parsedIv.getBytes(CHARSET));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(CHARSET), "AES");

            Cipher cipher = Cipher.getInstance(AESCipher.CIPHER_NAME);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(parsedEncrypted.getBytes(CHARSET));

            String hmacFromOriginal = hmac(new String(original, CHARSET), key);

            System.out.println("Decryption: Hmac from original: " + hmacFromOriginal);
            System.out.println("Decryption: Hmac algorithm: " + HMAC_ALGORITHM);
            System.out.println("Decryption: Hmac length: " + HMAC_LENGTH);

            if (!hmacFromOriginal.equals(parsedHmac)) {
                System.out.println("Decryption: ERROR: Invalid integrity check.");
                return null;
            }

            String decrypted =  new String(original, CHARSET);

            System.out.println("Decryption: Decrypted value: " + decrypted);

            return decrypted;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    static String digest(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(DIGEST_ALGORITHM);
        md.update(data.getBytes());
        return bytesToHex(md.digest());
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

    public static String hmac(String data, String key) throws NoSuchAlgorithmException {
        try {
            Mac sha256_HMAC = Mac.getInstance(HMAC_ALGORITHM);
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), HMAC_ALGORITHM);
            sha256_HMAC.init(secret_key);

            return bytesToHex(sha256_HMAC.doFinal(data.getBytes()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String generateIv() throws UnsupportedEncodingException {
        byte[] ivBytes = new byte[IV_LENGTH];
        SecureRandom random = new SecureRandom();
        random.nextBytes(ivBytes);

        return new String(ivBytes, CHARSET);
    }
}
