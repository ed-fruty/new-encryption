Encryption description:

Cipher method: `AES-256-CBC (AES/CBC/PKCS5PADDING)`

Password digest: `MD5` (32 bytes key length, to use 256 bit encryption)

Hmac algorithm: `HmacSHA256`

Encoding: `ISO-8859-1`

How it works:
-----

 - Encryption accepts only string payload to encrypt. 
If you want to encrypt array of data you should make string in query format (& as delimiter).

For example you want to encrypt `clientId` and `currency`

```java
String clientId = "111";
String currency = "USD";

String dataToEncrypt = "clientId=111" + clientId + "&currency=" + currency;
```

 - You must digest you key with `MD5` algorithm (see `digest` method)

```java
String originalKey = "MySecretKey";
String keyToEncrypt = digest(originalKey);


/* ------------------------------------------------------
 *      Example to make digest message from the scratch.
 * ------------------------------------------------------
 */

MessageDigest md = MessageDigest.getInstance("MD5");
md.update(originalKey.getBytes());
byte[] keyBytes = md.digest());

StringBuffer result = new StringBuffer();
for (byte byt : keyBytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));

String keyToEncrypt = result.toString();
```

 - Generate initialization vector (IV). 

It must be random 16 bytes (see `generateIv` method)

```java
String iv = generateIv();

/* -------------------------------------------
 *  Example to generate IV from the scratch.
 * -------------------------------------------
 */
byte[] ivBytes = new byte[16];
SecureRandom random = new SecureRandom();
random.nextBytes(ivBytes);

String iv = new String(ivBytes, CHARSET);
```

 - Prepare secret key and IV to use in cipher

```java
IvParameterSpec initVector = new IvParameterSpec(iv.getBytes(CHARSET));
SecretKeySpec skeySpec = new SecretKeySpec(keyToEncrypt.getBytes(CHARSET), "AES");
```

 - Initialize cipher instance with `AES/CBC/PKCS5PADDING`

```java
Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
cipher.init(Cipher.ENCRYPT_MODE, skeySpec, initVector);

```

 - Encrypt payload

```java
byte[] encryptedData = cipher.doFinal((dataToEncrypt.getBytes(CHARSET)));

```

 - Calculate integrity checksum of payload using `HmacSHA256` (see `hmac` method)

```java
String hmac = hmac(dataToEncrypt, keyToEncrypt);


/*------------------------------------------------------
 *   Example to genrate HMAC-SHA256 from the scratch
 *------------------------------------------------------
 */
Mac sha256HMAC = Mac.getInstance("HmacSHA256");
SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
sha256HMAC.init(secret_key);

byte[] hashBytes = sha256HMAC.doFinal(data.getBytes());
StringBuffer result = new StringBuffer();
for (byte byt : hashBytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));

String hmac = result.toString();    

```

 - Concat IV + HMAC + ENCRYPTED values

```java
String rawConcat =
      new String(iv.getBytes(CHARSET), CHARSET)
    + new String(hmac.getBytes(CHARSET), CHARSET)
    + new String(encryptedData, CHARSET);
```

 - Encode `rawConcat` with `base64`

```java
String hash = Base64.getEncoder().encodeToString(rawConcat.getBytes(CHARSET));
```

That's all.You should pass `hash` as encrypted data.

How to execute example:
-----

```bash

# compile
javac main/AESExample.java -d build

#run
cd build && java main.AESExample
```