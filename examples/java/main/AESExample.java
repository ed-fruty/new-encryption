package main;

import static ciphers.AESCipher.decrypt;
import static ciphers.AESCipher.encrypt;


public class AESExample {

    public static void main(String[] args) {

        String key = "0123456789abcd"; // it would be digest encoded to be 32 bytes (to use 256 bits encryption) 
        String payload = "client=117&something=value";

        String encrypted = encrypt(key, payload);

        System.out.println("");
        System.out.println("=============================================");
        System.out.println("");

        // Decrypt hash from PHP side
        // String decrypted = decrypt(key, new String("a9csR0pmmwIpuewb+rsLPzE1YzcyMTU1ZjEyMjdiNzg5ZTBmM2VlYjE1ZjIwYjRhMjA1MGI4MTEyYjk4NTFiY2EwYjhlZjE4MWM1YTMzMTZq9PrGDa13bCKXVE0LFjiNW2zIfqQEAWc41ZVEkiXpWw=="));
        

        String decrypted = decrypt(key, encrypted);
    }
}
