<?php

namespace Innovation\Encryption\Tests\Encryptors\OpenSsl;

use Innovation\Encryption\Encryptors\OpenSsl\OpenSslEncryption;
use Innovation\Encryption\ValueObjects\EncryptionKey;
use Innovation\Encryption\ValueObjects\Payload;
use PHPUnit\Framework\TestCase;

class OpenSslEncryptionTest extends TestCase
{
    public function testOne()
    {
        $encryption =  new OpenSslEncryption();

        $hash = $encryption->encrypt(new Payload('client=117&something=value'), new EncryptionKey('0123456789abcd'));

        var_dump($hash);

        $decrypted = $encryption->decrypt($hash, new EncryptionKey('0123456789abcd'));

        var_dump($decrypted);
    }
}